<?php

/**
 * @file
 */

// Composer Utility needs the versions key to be present.
return [
  'versions' => [
    'drupal/core' => [
      'name' => 'drupal/core',
      'version' => '9.8.0',
      'type' => 'drupal-core',
    ],
    'drupal/core-recommended' => [
      'name' => 'drupal/core-recommended',
      'version' => '9.8.0',
      'type' => 'drupal-core',
    ],
    'drupal/core-dev' => [
      'name' => 'drupal/core-dev',
      'version' => '9.8.0',
      'type' => 'drupal-core',
    ],
  ],
];
